// console.log('coba');
$(function(){
$('.ModalDetil').on('click',function(){
    $('#ModalLabel').html('Detail Data Mahasiswa');
    $('.modal-footer').hide();

    const id = $(this).data('id');
    $.ajax({
        url: 'http://localhost/web_lanjut/pertemuan11/phpmvc4/public/mahasiswa/getmahasiswa',
        data: {id: id},
        method: 'post',
        dataType: 'json',
        success: function(data){
            console.log(data);
            $('#nim').val(data.nim);
            $('#nama').val(data.nama);
            $('#telp').val(data.telp);
            $('#email').val(data.email);
        }
    });
});
$('.ModalTambah').on('click',function(){
    $('#ModalLabel').html('Tambah Data Mahasiswa');
    document.getElementById('frm').reset();
    $('.modal-footer').show();
    
});

$('.ModalUbah').on('click',function(){
    $('#ModalLabel').html('Ubah Data Mahasiswa');
    const id = $(this).data('id');
    $.ajax({
        url: 'http://localhost/web_lanjut/pertemuan11/phpmvc4/public/mahasiswa/getmahasiswa',
        data: {id: id},
        method: 'post',
        dataType: 'json',
        success: function(data){
            console.log(data);
            $('#id').val(data.id);
            $('#nim').val(data.nim);
            $('#nama').val(data.nama);
            $('#telp').val(data.telp);
            $('#email').val(data.email);
        }
    });
    $('.modal-body form').attr('action','http://localhost/web_lanjut/pertemuan11/phpmvc4/public/mahasiswa/ubah');
    $('.modal-footer').show();
    $('.modal-footer button[type=submit]').html('Ubah');
});

// MODAL DOSEN
$('.ModalDetil1').on('click',function(){
    // console.log('Masuk Modal Detil');
    $('#ModalLabel').html('Detail Data Dosen');
    $('.modal-footer').hide();

    const id = $(this).data('id');
    // console.log(id);
    $.ajax({
        url: 'http://localhost/web_lanjut/pertemuan11/phpmvc4/public/dosen/getdosen',
        data: {id: id},
        method: 'post',
        dataType: 'json',
        success: function(data){
            console.log(data);
            $('#NIDN').val(data.NIDN);
            $('#nama').val(data.nama);
            $('#telp').val(data.telp);
        }
    });
});
$('.ModalTambah1').on('click',function(){
    $('#ModalLabel').html('Tambah Data Dosen');
    document.getElementById('frm1').reset();
    $('.modal-footer').show();
});
});