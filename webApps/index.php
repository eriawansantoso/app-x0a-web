<!DOCTYPE html>
<html lang="en">
<html>
    <head>
        <title> SI Olah Data SIswa</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="jquery/jquery-3.4.1.min.js"></script>
        <script src="jquery/jquery-ui.min.js"></script>
    </head>
    <body style="background-image: url('images/sekolah.jpg');">
    <div class="container" style="margin-top: 5%;" >
        <div class="jumbotron">
            <div class="container" style="margin-top: 5%;">
                <h1 class="display-5" text-align="center">Sistem Informasi Pengolahan Data Siswa SMA</h1>
            </div>
            <hr class="my-5">
            <div class="container">
                <p>silahkan masuk kedalam website untuk melihat data anda</p>
                <a class="btn btn-primary btn-lg" href="prodi.php" role="button">Prodi</a>
                <a class="btn btn-primary btn-lg" href="mahasiswa.php" role="button">Mahasiswa</a>
            </div>
        </div>
    </div>
    </body>
</html>