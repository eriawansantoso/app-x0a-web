/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100140
 Source Host           : localhost:3306
 Source Schema         : kampus

 Target Server Type    : MySQL
 Target Server Version : 100140
 File Encoding         : 65001

 Date: 30/04/2020 15:02:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE `mahasiswa`  (
  `nim` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_prodi` int(255) NOT NULL,
  `photos` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`nim`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mahasiswa
-- ----------------------------
INSERT INTO `mahasiswa` VALUES ('1401001', 'Imam', 1, 'imam.jpg', 'gampeng');
INSERT INTO `mahasiswa` VALUES ('1402001', 'Nurul', 2, 'nurul.jpg', 'ngadisimo');
INSERT INTO `mahasiswa` VALUES ('1403001', 'Mahmud', 3, 'mahmud.jpg', 'semampir');
INSERT INTO `mahasiswa` VALUES ('1501001', 'Khoirul', 2, 'khoirul.jpg', 'ngebrak');
INSERT INTO `mahasiswa` VALUES ('1502001', 'Junaedi', 3, 'junaedi.jpg', 'semampir');
INSERT INTO `mahasiswa` VALUES ('1503001', 'Ayu', 1, 'ayu.jpg', 'bali');

-- ----------------------------
-- Table structure for prodi
-- ----------------------------
DROP TABLE IF EXISTS `prodi`;
CREATE TABLE `prodi`  (
  `id_prodi` int(255) NOT NULL AUTO_INCREMENT,
  `nama_prodi` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_prodi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of prodi
-- ----------------------------
INSERT INTO `prodi` VALUES (1, 'Informatika');
INSERT INTO `prodi` VALUES (2, 'Akuntansi');
INSERT INTO `prodi` VALUES (3, 'Mesin');

SET FOREIGN_KEY_CHECKS = 1;
